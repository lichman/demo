package com.example.demo.model;

public enum ApplicationStatus {
    ALLOW, REJECT, MANUAL
}
