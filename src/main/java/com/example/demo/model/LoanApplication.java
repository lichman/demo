package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Date;

@Data
public class LoanApplication {

    @NonNull
    private String personalID;
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;

    private LocalDate birthDate;
    private String employer;
    private BigDecimal salary;
    private BigDecimal liability;
    private BigDecimal requestedAmount;
    private Integer requestedTerm;

    private ApplicationStatus status;

    @JsonIgnore
    private Integer profit;



}
