package com.example.demo.services;

import com.example.demo.model.ApplicationStatus;
import com.example.demo.model.LoanApplication;

public interface ScoringService {

      Integer getScore(LoanApplication application);

}
