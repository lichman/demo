package com.example.demo.services;

import com.example.demo.model.ApplicationStatus;
import com.example.demo.model.LoanApplication;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public class ScoringServiceLiteImpl implements ScoringService {
    @Override
    public Integer getScore(LoanApplication application) {

        BigDecimal dateSum = BigDecimal.valueOf(birthdaySum(application.getBirthDate()));

        BigDecimal nameSum = BigDecimal.valueOf(sumOfName(application.getFirstName()));

        BigDecimal finalSalary = application.getSalary().multiply(BigDecimal.valueOf(1.5));

        BigDecimal finalMonth = application.getLiability().multiply(BigDecimal.valueOf(3));

        BigDecimal subst = finalSalary.subtract(finalMonth);
        int score = nameSum.add(subst.add(dateSum)).intValue();

        String template = "Rustaveli";

        if(LevenshteinDistance.getDefaultInstance().apply(template,  application.getLastName()) < 5 ) {
            score = score + 100;
        }

        return score;
    }

    private Integer sumOfName(String name) {
       return name.chars().map( i -> i - ('a' - 1)).sum();
    }

    private Integer birthdaySum(LocalDate date) {
        return date.getYear() - date.getMonthValue() - date.getDayOfYear();

    }

}
