package com.example.demo.algo;

import java.util.Optional;
import java.util.Random;

class Node<T extends Comparable<T>> {
    public T value;
    public Optional<Node<T>> left = Optional.empty();
    public Optional<Node<T>> right = Optional.empty();

    Node(T value) {
        this.value = value;
    }

    public void add(T v) {
        Optional<Node<T>> leaf;
        if (this.value.compareTo(v) > 0) {
            if (!this.left.isPresent()) {
                Node<T> node = new Node<T>(v);
                this.left = Optional.of(node);
                return;
            }
            leaf = this.left;
        } else if (this.value.compareTo(v) < 0) {
            if (!this.right.isPresent()) {
                Node<T> node = new Node<T>(v);
                this.right = Optional.of(node);
                return;
            }
            leaf = this.right;
        } else {
            this.value = v;
            return;
        }


        leaf.get().add(v);

    }

    public void debug() {
        if (left.isPresent()) {
            System.out.print("left: ");
            left.get().debug();
        }
        System.out.println(this.value);

        if (right.isPresent()) {
            System.out.print("right: ");
            right.get().debug();
        }
    }

    public Boolean exist(T value) {
        return true;

    }

    public void delete(T value) {

    }

}


public class Tree<T extends Comparable<T>> {

    private Optional<Node<T>> root = Optional.empty();

    public void add(T value) {
        if (root.isPresent()) {
            root.get().add(value);
        } else {
            root = Optional.of(new Node(value));
        }

    }

    public void debug() {
        if (root.isPresent()) {
            root.get().debug();
        } else {
            System.out.println("Empty tree");
        }

    }


    public static void main(String[] args) {

        final Tree<Integer> tree = new Tree();
        Random random = new Random();


        random.ints(0, 10).limit(50).forEach(i -> tree.add(i));
         Node<Integer> root = new Node(6);

         root.left = Optional.of(new Node(4));

         root.right = Optional.of(new Node(9));

         Node<Integer> leaf = root.left.get();

         tree.root = Optional.of(root);

         tree.add(5);




        tree.debug();


    }
}
