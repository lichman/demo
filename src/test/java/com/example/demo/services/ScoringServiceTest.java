package com.example.demo.services;

import com.example.demo.model.ApplicationStatus;
import com.example.demo.model.LoanApplication;
import org.junit.Assert;

import static org.junit.Assert.*;

public class ScoringServiceTest {

    @org.junit.Test
    public void testAllowStatus() {
        LoanApplication application = new LoanApplication();

        application.setEmployer();

        ScoringService service = new ScoringServiceLiteImpl();

        Integer status = service.getScore(application);

        assertEquals(Integer.valueOf(2400), status);
    }

    @org.junit.Test
    public void testRejectStatus() {
        LoanApplication application = new LoanApplication();

        application.setEmployer();

        ScoringService service = new ScoringServiceLiteImpl();

        Integer status = service.getScore(application);

        assertEquals(ApplicationStatus.REJECT, status);
    }


    @org.junit.Test
    public void testManualStatus() {
        LoanApplication application = new LoanApplication();

        application.setEmployer();

        ScoringService service = new ScoringServiceLiteImpl();

        Integer status = service.getScore(application);

        assertEquals(ApplicationStatus.MANUAL, status);
    }


    @org.junit.Test(expected = NullPointerException.class)
    public void testNullApplication() {
        ScoringService service = new ScoringServiceLiteImpl();
        service.getScore(null);
    }



    @org.junit.Test
    public void testSpecCaseStatus() {
        LoanApplication application = new LoanApplication();

        application.setEmployer();

        ScoringService service = new ScoringServiceLiteImpl();

        Integer status = service.getScore(application);

        assertEquals(ApplicationStatus.REJECT, status);
    }


}