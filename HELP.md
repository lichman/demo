# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/maven-plugin/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)



Model
User.java 9
LoanApplication.java 1
Enum ApplicationStatus.java 2
Enum SortFields 3

Repository
Users 10
LoanApplicationList 4
LoanApplicationCRUD 5

Service
ScoringService 6
UserService 11

Controllers
AuthController 13
AdminControl 12

ApplicationController 7

AdminApplicationController 8
  
